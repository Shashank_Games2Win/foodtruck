﻿using UnityEngine;
using System.Collections;

public class Refiller : MonoBehaviour
{

	public IngredientsController[] ingredients;
	public int factoryID;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		TouchManager ();
	}

	void TouchManager ()
	{
		if (Input.GetMouseButtonUp (0)) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast ((ray), out hit) && hit.collider.name.Contains ("Ingredient")) {
				ingredients [factoryID - 1].Refill (factoryID);
			}
		}
	}
}
