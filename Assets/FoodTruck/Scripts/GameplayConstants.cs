﻿using UnityEngine;
using System.Collections;

public class GameplayConstants : MonoBehaviour
{

	public static int availableTimeForLevel = 30;
	public static int secondsDelayBetweenCustomers = 5;
	public static bool isDirectServe = false;

	public static int[] sideRequestPrices = new int[8] { 30, 35, 70, 90, 110, 130, 150, 170 };
}
